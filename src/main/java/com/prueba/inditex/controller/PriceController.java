package com.prueba.inditex.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.inditex.entity.Price;
import com.prueba.inditex.entity.PriceResponse;
import com.prueba.inditex.entity.PriceResquest;
import com.prueba.inditex.service.PriceServiceImpl;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.servers.Server;

@RestController
@OpenAPIDefinition(
		  info = @Info(
		  title = "Prueba Inditex",
		  description = " " +
		    "Prueba realizada con Spring Boot + base de datos H2 + JPA + Swagger 3 + Java 11 + Junit ",
		  contact = @Contact(
		    name = "Jesús Alejandro Benítez Pedrero", 
		    url = "https://www.linkedin.com/in/jes%C3%BAs-alejandro-ben%C3%ADtez-pedrero-401705102/", 
		    email = "indarecio2@gmail.com"
		  ),
		  license = @License(
		    name = "Proyecto en repositorio GitLab", 
		    url = "https://gitlab.com/indarecio2/inditex_prueba")),
		  servers = @Server(url = "http://localhost:8080")
		)
public class PriceController {

	@Autowired
	PriceServiceImpl priceService;

	@Operation(summary = "Obtener la lista de registros de la tabla PRICE")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Found list prices", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Price.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
			@ApiResponse(responseCode = "404", description = "List price not found", content = @Content) })
	@GetMapping("/prices")
	public List<Price> getAllPrices() {
		return priceService.getAllPrices();
	}

	@Operation(summary = "Obtener registros por ID de la tabla PRICE")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Found price", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Price.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
			@ApiResponse(responseCode = "404", description = "Price not found", content = @Content) })
	@GetMapping("/prices/{id}")
	public Price getPrice(@PathVariable("id") Long id) {
		return priceService.getPriceById(id);
	}

	@Operation(summary = "Eliminar un registro por ID de la tabla PRICE")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Delete the price", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Price.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
			@ApiResponse(responseCode = "404", description = "Price not found", content = @Content) })
	@DeleteMapping("/prices/{id}")
	public void deletePrice(@PathVariable("id") Long id) {
		priceService.delete(id);
	}

	@Operation(summary = "Actualizar un registro de la tabla PRICE")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Update the price", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Price.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
			@ApiResponse(responseCode = "404", description = "Price not found", content = @Content) })
	@PostMapping("/prices")
	public long savePrice(
			@RequestBody @Valid Price price) {
		priceService.saveOrUpdate(price);
		return price.getId();
	}

	@Operation(summary = "Obtener el precio final")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Update the price", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = PriceResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
			@ApiResponse(responseCode = "404", description = "Price not found", content = @Content) })
	@PostMapping("/price")
	public PriceResponse applyPrice(
			@RequestBody PriceResquest price) throws Exception {
		return priceService.applyPrice(price).get(priceService.applyPrice(price).size()-1);
	}

}
