package com.prueba.inditex.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.inditex.entity.Price;
import com.prueba.inditex.entity.PriceRepository;
import com.prueba.inditex.entity.PriceResponse;
import com.prueba.inditex.entity.PriceResquest;

@Service
public class PriceServiceImpl implements PriceService{
	
    @Autowired
    PriceRepository priceRepository;
    
    @Override
	public List<Price> getAllPrices() {
        List<Price> prices = new ArrayList<Price>();
        priceRepository.findAll().forEach(price -> prices.add(price));
        return prices;
    }
    
    @Override
    public Price getPriceById(Long id) {
        return priceRepository.findById(id).get();
    }

    @Override
    public void saveOrUpdate(Price price) {
    	priceRepository.save(price);
    }

    @Override
    public void delete(Long id) {
    	priceRepository.deleteById(id);
    }
    
    @Override
    public List<PriceResponse> applyPrice(PriceResquest priceResquest) throws Exception {
    	List<PriceResponse> listResponse = new ArrayList<>();
    	
        List<Price> res = priceRepository.findByBrand_idAndproduct_idAndDate(priceResquest.getBrand_id(), priceResquest.getProduct_id(), priceResquest.getFecha());
        res.sort(Comparator.comparing(Price::getId).thenComparing(Price::getPriority));
        
    	for (Price price : res) {
    		PriceResponse response = new PriceResponse();
    		response.setBrand_id(price.getBrand_id());
    		response.setProduct_id(price.getProduct_id());
    		response.setPrice(price.getPrice());
    		response.setPrice_list(price.getPrice_list());
    		response.setStart_date(price.getStart_date());
    		response.setEnd_date(price.getEnd_date());
    		listResponse.add(response);
		}
		return listResponse;
    	
    }

}
