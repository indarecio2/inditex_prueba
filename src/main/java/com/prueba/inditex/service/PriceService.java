package com.prueba.inditex.service;

import java.util.List;

import com.prueba.inditex.entity.Price;
import com.prueba.inditex.entity.PriceResponse;
import com.prueba.inditex.entity.PriceResquest;

public interface PriceService {
	
	public List<Price> getAllPrices();
    public Price getPriceById(Long id);
    public void saveOrUpdate(Price price);
    public void delete(Long id);
    public List<PriceResponse> applyPrice(PriceResquest priceResquest) throws Exception;
}
