package com.prueba.inditex;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.prueba.inditex.entity.Price;
import com.prueba.inditex.entity.PriceRepository;
import com.prueba.inditex.util.Constantes;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
public class InditexPruebaApplication {

	public static void main(String[] args) throws ParseException {
		
		ConfigurableApplicationContext configurableApplicationContext =
		SpringApplication.run(InditexPruebaApplication.class, args);
		PriceRepository priceRepository = configurableApplicationContext.getBean(PriceRepository.class);
		SimpleDateFormat formateador=null;	
        formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);
        Price price = new Price(1,formateador.parse("2020-06-14 00.00.00"),formateador.parse("2020-12-31 23.59.59"),1,35455,0,35.50,"EUR");
        Price price1 = new Price(1,formateador.parse("2020-06-14 15.00.00"),formateador.parse("2020-06-14 18.30.00"),2,35455,1,25.45,"EUR");
        Price price2 = new Price(1,formateador.parse("2020-06-15 00.00.00"),formateador.parse("2020-06-15 11.00.00"),3,35455,1,30.50,"EUR");
        Price price3 = new Price(1,formateador.parse("2020-06-15 16.00.00"),formateador.parse("2020-12-31 23.59.59"),4,35455,1,38.95,"EUR");
		priceRepository.save(price);
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
	}

}
