package com.prueba.inditex.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {
	
	@Query("SELECT p FROM Price p WHERE p.brand_id=?1 AND p.product_id=?2 AND ?3 BETWEEN p.start_date AND p.end_date")
	public List<Price> findByBrand_idAndproduct_idAndDate(Integer brand_id, Integer product_id,  Date fecha);
}
