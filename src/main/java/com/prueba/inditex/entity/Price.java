package com.prueba.inditex.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "PRICES")
public class Price {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "BRAND_ID", nullable = false)
	private Integer brand_id;

	@Column(name = "START_DATE", nullable = false)
	private Date start_date;

	@Column(name = "END_DATE", nullable = false)
	private Date end_date;

	@Column(name = "PRICE_LIST", nullable = false)
	private Integer price_list;

	@Column(name = "PRODUCT_ID", nullable = false)
	private Integer product_id;

	@Column(name = "PRIORITY", nullable = false)
	private Integer priority;

	@Column(name = "PRICE", nullable = false)
	private Double price;

	@Column(name = "CURR", nullable = false, length = 4)
	private String curr;

	public Price(Integer brand_id, Date start_date, Date end_date, Integer price_list, Integer product_id,
			Integer priority, Double price, String curr) {
		super();
		this.brand_id = brand_id;
		this.start_date = start_date;
		this.end_date = end_date;
		this.price_list = price_list;
		this.product_id = product_id;
		this.priority = priority;
		this.price = price;
		this.curr = curr;
	}

	public Price() {
		super();
	}

	public Price(Long id, Integer brand_id, Date start_date, Date end_date, Integer price_list, Integer product_id,
			Integer priority, Double price, String curr) {
		super();
		this.id = id;
		this.brand_id = brand_id;
		this.start_date = start_date;
		this.end_date = end_date;
		this.price_list = price_list;
		this.product_id = product_id;
		this.priority = priority;
		this.price = price;
		this.curr = curr;
	}

}
