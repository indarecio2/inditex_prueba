package com.prueba.inditex.entity;

import java.util.Date;

import lombok.Data;

@Data
public class PriceResquest {

	private Date fecha;
	private Integer product_id;
	private Integer brand_id;
	
	public PriceResquest(Date fecha, Integer product_id, Integer brand_id) {
		super();
		this.fecha = fecha;
		this.product_id = product_id;
		this.brand_id = brand_id;
	}

}
