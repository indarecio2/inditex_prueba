package com.prueba.inditex.entity;

import java.util.Date;

import lombok.Data;

@Data
public class PriceResponse {

	private Integer product_id;
	private Integer brand_id;
	private Integer price_list;
	private Date start_date;
	private Date end_date;
	private Double price;
	
	public PriceResponse(Integer product_id, Integer brand_id, Integer price_list, Date start_date, Date end_date,
			Double price) {
		super();
		this.product_id = product_id;
		this.brand_id = brand_id;
		this.price_list = price_list;
		this.start_date = start_date;
		this.end_date = end_date;
		this.price = price;
	}

	public PriceResponse() {
		super();
	}
	
	

}
