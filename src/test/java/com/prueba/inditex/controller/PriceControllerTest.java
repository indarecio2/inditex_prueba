package com.prueba.inditex.controller;

import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.prueba.inditex.entity.Price;
import com.prueba.inditex.entity.PriceRepository;
import com.prueba.inditex.entity.PriceResponse;
import com.prueba.inditex.entity.PriceResquest;
import com.prueba.inditex.service.PriceServiceImpl;
import com.prueba.inditex.util.Constantes;

public class PriceControllerTest {

	@Mock
	PriceRepository priceRepository;

	@Mock
	PriceServiceImpl priceService;

	@InjectMocks
	PriceController priceController;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@DisplayName("Test 1 -test Controller")
	void applyPriceControllerTest1() throws Exception {

		SimpleDateFormat formateador = null;
		Integer brand_id = 1;
		Integer product_id = 1;
		Double precio = 35.5;
		List<Price> pricesList = new ArrayList<>();
		List<PriceResponse> ListResponse = new ArrayList<>();
		
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);

		Price price1 = new Price(Long.parseLong("1"), 1, formateador.parse("2020-06-14 00.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 1, 35455, 0, 35.50, "EUR");
		Price price2 = new Price(Long.parseLong("2"), 1, formateador.parse("2020-06-14 15.00.00"),
				formateador.parse("2020-06-14 18.30.00"), 2, 35455, 1, 25.45, "EUR");
		Price price3 = new Price(Long.parseLong("3"), 1, formateador.parse("2020-06-15 00.00.00"),
				formateador.parse("2020-06-15 11.00.00"), 3, 35455, 1, 30.50, "EUR");
		Price price4 = new Price(Long.parseLong("4"), 1, formateador.parse("2020-06-15 16.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 4, 35455, 1, 38.95, "EUR");
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
		priceRepository.save(price4);
		
		pricesList.add(price1);
		
		when(priceRepository.findByBrand_idAndproduct_idAndDate(brand_id, product_id,
				formateador.parse("2020-06-14 10.00.00"))).thenReturn(pricesList);
		
		Integer product_idR = pricesList.get(0).getProduct_id();
		Integer brand_idR = pricesList.get(0).getBrand_id();
		Integer price_list = pricesList.get(0).getPrice_list();
		Date start_date = pricesList.get(0).getStart_date();
		Date end_date = pricesList.get(0).getEnd_date();
		Double price = pricesList.get(0).getPrice();
		
		PriceResponse response = new PriceResponse(product_idR, brand_idR, price_list, start_date, end_date, price);
		ListResponse.add(response);
		
		PriceResquest request = new PriceResquest(formateador.parse("2020-06-14 10.00.00"), product_id, brand_id);
		
		when(priceService.applyPrice(request)).thenReturn(ListResponse);

		PriceResponse res = priceController.applyPrice(request);

		Assertions.assertEquals(res.getPrice(), precio);

	}
	
	@Test
	@DisplayName("Test 2 -test Controller")
	void applyPriceControllerTest2() throws Exception {

		SimpleDateFormat formateador = null;
		Integer brand_id = 1;
		Integer product_id = 1;
		Double precio = 25.45;
		List<Price> pricesList = new ArrayList<>();
		List<PriceResponse> ListResponse = new ArrayList<>();
		
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);

		Price price1 = new Price(Long.parseLong("1"), 1, formateador.parse("2020-06-14 00.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 1, 35455, 0, 35.50, "EUR");
		Price price2 = new Price(Long.parseLong("2"), 1, formateador.parse("2020-06-14 15.00.00"),
				formateador.parse("2020-06-14 18.30.00"), 2, 35455, 1, 25.45, "EUR");
		Price price3 = new Price(Long.parseLong("3"), 1, formateador.parse("2020-06-15 00.00.00"),
				formateador.parse("2020-06-15 11.00.00"), 3, 35455, 1, 30.50, "EUR");
		Price price4 = new Price(Long.parseLong("4"), 1, formateador.parse("2020-06-15 16.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 4, 35455, 1, 38.95, "EUR");
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
		priceRepository.save(price4);
		
		pricesList.add(price2);
		
		when(priceRepository.findByBrand_idAndproduct_idAndDate(brand_id, product_id,
				formateador.parse("2020-06-14 16.00.00"))).thenReturn(pricesList);
		
		Integer product_idR = pricesList.get(0).getProduct_id();
		Integer brand_idR = pricesList.get(0).getBrand_id();
		Integer price_list = pricesList.get(0).getPrice_list();
		Date start_date = pricesList.get(0).getStart_date();
		Date end_date = pricesList.get(0).getEnd_date();
		Double price = pricesList.get(0).getPrice();
		
		PriceResponse response = new PriceResponse(product_idR, brand_idR, price_list, start_date, end_date, price);
		ListResponse.add(response);
		
		PriceResquest request = new PriceResquest(formateador.parse("2020-06-14 16.00.00"), product_id, brand_id);
		
		when(priceService.applyPrice(request)).thenReturn(ListResponse);

		PriceResponse res = priceController.applyPrice(request);

		Assertions.assertEquals(res.getPrice(), precio);

	}
	
	@Test
	@DisplayName("Test 3 -test Controller")
	void applyPriceControllerTest3() throws Exception {

		SimpleDateFormat formateador = null;
		Integer brand_id = 1;
		Integer product_id = 1;
		Double precio = 35.5;
		List<Price> pricesList = new ArrayList<>();
		List<PriceResponse> ListResponse = new ArrayList<>();
		
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);

		Price price1 = new Price(Long.parseLong("1"), 1, formateador.parse("2020-06-14 00.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 1, 35455, 0, 35.50, "EUR");
		Price price2 = new Price(Long.parseLong("2"), 1, formateador.parse("2020-06-14 15.00.00"),
				formateador.parse("2020-06-14 18.30.00"), 2, 35455, 1, 25.45, "EUR");
		Price price3 = new Price(Long.parseLong("3"), 1, formateador.parse("2020-06-15 00.00.00"),
				formateador.parse("2020-06-15 11.00.00"), 3, 35455, 1, 30.50, "EUR");
		Price price4 = new Price(Long.parseLong("4"), 1, formateador.parse("2020-06-15 16.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 4, 35455, 1, 38.95, "EUR");
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
		priceRepository.save(price4);
		
		pricesList.add(price1);
		
		when(priceRepository.findByBrand_idAndproduct_idAndDate(brand_id, product_id,
				formateador.parse("2020-06-14 21.00.00"))).thenReturn(pricesList);
		
		Integer product_idR = pricesList.get(0).getProduct_id();
		Integer brand_idR = pricesList.get(0).getBrand_id();
		Integer price_list = pricesList.get(0).getPrice_list();
		Date start_date = pricesList.get(0).getStart_date();
		Date end_date = pricesList.get(0).getEnd_date();
		Double price = pricesList.get(0).getPrice();
		
		PriceResponse response = new PriceResponse(product_idR, brand_idR, price_list, start_date, end_date, price);
		ListResponse.add(response);
		
		PriceResquest request = new PriceResquest(formateador.parse("2020-06-14 21.00.00"), product_id, brand_id);
		
		when(priceService.applyPrice(request)).thenReturn(ListResponse);

		PriceResponse res = priceController.applyPrice(request);

		Assertions.assertEquals(res.getPrice(), precio);

	}
	
	@Test
	@DisplayName("Test 4 -test Controller")
	void applyPriceControllerTest4() throws Exception {

		SimpleDateFormat formateador = null;
		Integer brand_id = 1;
		Integer product_id = 1;
		Double precio = 35.5;
		List<Price> pricesList = new ArrayList<>();
		List<PriceResponse> ListResponse = new ArrayList<>();
		
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);

		Price price1 = new Price(Long.parseLong("1"), 1, formateador.parse("2020-06-14 00.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 1, 35455, 0, 35.50, "EUR");
		Price price2 = new Price(Long.parseLong("2"), 1, formateador.parse("2020-06-14 15.00.00"),
				formateador.parse("2020-06-14 18.30.00"), 2, 35455, 1, 25.45, "EUR");
		Price price3 = new Price(Long.parseLong("3"), 1, formateador.parse("2020-06-15 00.00.00"),
				formateador.parse("2020-06-15 11.00.00"), 3, 35455, 1, 30.50, "EUR");
		Price price4 = new Price(Long.parseLong("4"), 1, formateador.parse("2020-06-15 16.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 4, 35455, 1, 38.95, "EUR");
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
		priceRepository.save(price4);
		
		pricesList.add(price1);
		
		when(priceRepository.findByBrand_idAndproduct_idAndDate(brand_id, product_id,
				formateador.parse("2020-06-15 10.00.00"))).thenReturn(pricesList);
		
		Integer product_idR = pricesList.get(0).getProduct_id();
		Integer brand_idR = pricesList.get(0).getBrand_id();
		Integer price_list = pricesList.get(0).getPrice_list();
		Date start_date = pricesList.get(0).getStart_date();
		Date end_date = pricesList.get(0).getEnd_date();
		Double price = pricesList.get(0).getPrice();
		
		PriceResponse response = new PriceResponse(product_idR, brand_idR, price_list, start_date, end_date, price);
		ListResponse.add(response);
		
		PriceResquest request = new PriceResquest(formateador.parse("2020-06-15 10.00.00"), product_id, brand_id);
		
		when(priceService.applyPrice(request)).thenReturn(ListResponse);

		PriceResponse res = priceController.applyPrice(request);

		Assertions.assertEquals(res.getPrice(), precio);

	}
	
	@Test
	@DisplayName("Test 5 -test Controller")
	void applyPriceControllerTest5() throws Exception {

		SimpleDateFormat formateador = null;
		Integer brand_id = 1;
		Integer product_id = 1;
		Double precio = 38.95;
		List<Price> pricesList = new ArrayList<>();
		List<PriceResponse> ListResponse = new ArrayList<>();
		
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);

		Price price1 = new Price(Long.parseLong("1"), 1, formateador.parse("2020-06-14 00.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 1, 35455, 0, 35.50, "EUR");
		Price price2 = new Price(Long.parseLong("2"), 1, formateador.parse("2020-06-14 15.00.00"),
				formateador.parse("2020-06-14 18.30.00"), 2, 35455, 1, 25.45, "EUR");
		Price price3 = new Price(Long.parseLong("3"), 1, formateador.parse("2020-06-15 00.00.00"),
				formateador.parse("2020-06-15 11.00.00"), 3, 35455, 1, 30.50, "EUR");
		Price price4 = new Price(Long.parseLong("4"), 1, formateador.parse("2020-06-15 16.00.00"),
				formateador.parse("2020-12-31 23.59.59"), 4, 35455, 1, 38.95, "EUR");
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
		priceRepository.save(price4);
		
		pricesList.add(price4);
		
		when(priceRepository.findByBrand_idAndproduct_idAndDate(brand_id, product_id,
				formateador.parse("2020-06-16 21.00.00"))).thenReturn(pricesList);
		
		Integer product_idR = pricesList.get(0).getProduct_id();
		Integer brand_idR = pricesList.get(0).getBrand_id();
		Integer price_list = pricesList.get(0).getPrice_list();
		Date start_date = pricesList.get(0).getStart_date();
		Date end_date = pricesList.get(0).getEnd_date();
		Double price = pricesList.get(0).getPrice();
		
		PriceResponse response = new PriceResponse(product_idR, brand_idR, price_list, start_date, end_date, price);
		ListResponse.add(response);
		
		PriceResquest request = new PriceResquest(formateador.parse("2020-06-16 21.00.00"), product_id, brand_id);
		
		when(priceService.applyPrice(request)).thenReturn(ListResponse);

		PriceResponse res = priceController.applyPrice(request);

		Assertions.assertEquals(res.getPrice(), precio);

	}
}
