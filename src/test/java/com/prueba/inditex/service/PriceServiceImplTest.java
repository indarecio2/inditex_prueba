package com.prueba.inditex.service;

import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.prueba.inditex.entity.Price;
import com.prueba.inditex.entity.PriceRepository;
import com.prueba.inditex.entity.PriceResponse;
import com.prueba.inditex.entity.PriceResquest;
import com.prueba.inditex.util.Constantes;

public class PriceServiceImplTest {

	@Mock
	PriceRepository priceRepository;

	@InjectMocks
	PriceServiceImpl priceService;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@DisplayName("Obtener todos los precios -test Service")
	void getAllPricesTest() throws Exception {

		SimpleDateFormat formateador = null;
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);

		Price price1 = new Price(1, formateador.parse("2020-06-14 00.00.00"), formateador.parse("2020-12-31 23.59.59"),
				1, 35455, 0, 35.50, "EUR");
		Price price2 = new Price(1, formateador.parse("2020-06-14 15.00.00"), formateador.parse("2020-06-14 18.30.00"),
				2, 35455, 1, 25.45, "EUR");
		Price price3 = new Price(1, formateador.parse("2020-06-15 00.00.00"), formateador.parse("2020-06-15 11.00.00"),
				3, 35455, 1, 30.50, "EUR");
		Price price4 = new Price(1, formateador.parse("2020-06-15 16.00.00"), formateador.parse("2020-12-31 23.59.59"),
				4, 35455, 1, 38.95, "EUR");

		List<Price> pricesList = new ArrayList<>();

		pricesList.add(price1);
		pricesList.add(price2);
		pricesList.add(price3);
		pricesList.add(price4);
		
		when(priceRepository.findAll()).thenReturn(pricesList);

		Assertions.assertEquals(4, priceService.getAllPrices().size());

	}
	@Test
	@DisplayName("Obtener un id -test Service")
	void getPriceByIdTest() throws Exception {
		
		SimpleDateFormat formateador = null;
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);
		
		Long id = Long.parseLong("1");
		Price price = new Price(1, formateador.parse("2020-06-14 00.00.00"), formateador.parse("2020-12-31 23.59.59"),
				1, 35455, 0, 35.50, "EUR");
		
		when(priceRepository.findById(id)).thenReturn(Optional.of(price));
		
		Assertions.assertEquals(priceService.getPriceById(id).getBrand_id(), 1);
		Assertions.assertEquals(priceService.getPriceById(id).getProduct_id(), 35455);
	}
	
	@Test
	@DisplayName("Obtener el precio final -test Service")
	void applyPriceTest() throws Exception {

		SimpleDateFormat formateador = null;
		formateador = new SimpleDateFormat(Constantes.ACTIVIDAD_FORMATO_FECHA);
		
        Price price1 = new Price(Long.parseLong("1"),1,formateador.parse("2020-06-14 00.00.00"),formateador.parse("2020-12-31 23.59.59"),1,35455,0,35.50,"EUR");
        Price price2 = new Price(Long.parseLong("2"),1,formateador.parse("2020-06-14 15.00.00"),formateador.parse("2020-06-14 18.30.00"),2,35455,1,25.45,"EUR");
        Price price3 = new Price(Long.parseLong("3"),1,formateador.parse("2020-06-15 00.00.00"),formateador.parse("2020-06-15 11.00.00"),3,35455,1,30.50,"EUR");
        Price price4 = new Price(Long.parseLong("4"),1,formateador.parse("2020-06-15 16.00.00"),formateador.parse("2020-12-31 23.59.59"),4,35455,1,38.95,"EUR");
		priceRepository.save(price1);
		priceRepository.save(price2);
		priceRepository.save(price3);
		priceRepository.save(price4);
		
		List<Price> pricesList = new ArrayList<>();

		pricesList.add(price1);
		pricesList.add(price2);
		pricesList.add(price3);
		pricesList.add(price4);

		Integer brand_id = 1;
		Integer product_id = 1;

		Mockito.when(priceRepository.findByBrand_idAndproduct_idAndDate(brand_id, product_id,
				formateador.parse("2020-06-14 10.00.00"))).thenReturn(pricesList);

		PriceResquest request = new PriceResquest(formateador.parse("2020-06-14 10.00.00"), product_id, brand_id);

		List<PriceResponse> responseList = priceService.applyPrice(request);

		Assertions.assertEquals(responseList.size(), pricesList.size());
	

	}

}
